<?php

set_time_limit(360000);
ini_set('memory_limit', '-1');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$time_start = microtime(true);
require_once("class/AutoLoad.php");
require_once("helper/functions.php");
Config::readConfig();

main();

function main()
{
    (new HubsaleService())->getAllVendas();
    $clientes = Cliente::getAll();
    foreach ($clientes as $cliente) {
        $cliente->retorno_leadlovers = Pagelovers::send($cliente);
        $cliente->update();
    }
}
