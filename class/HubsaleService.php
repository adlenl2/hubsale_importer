<?php

class HubsaleService
{
    private $token = '';

    private function getToken()
    {
        if ($this->token != '') {
            return $this->token;
        }
        $url = 'https://api.hub.sale/2.0/auth/';
        $params = ['grant_type' => "client_credentials"];

        $clientID = "0689a1ecb1617e30a3c09141bda59aa9";
        $clientSecret = "0f096d389e47818d5f45055aad6ea7e9";
        $authString = base64_encode($clientID . ":" . $clientSecret);
        $headers = [
            "Content-type: application/x-www-form-urlencoded",
            "Authorization: Basic $authString"
        ];

        $response = CustomCurl::sendRequest($url, $params, $headers);
        $responseObj = json_decode($response);
        if ($responseObj->success === true) {
            $this->token = $responseObj->data->token;
        } else {
            exit("Erro ao buscar o token");
        }
        return $this->token;
    }
    public function getAllVendas()
    {
        $month = strtotime('2021-06-01');
        $end = strtotime('2025-01-01');
        while ($month < $end) {
            $this->getVendas(date('m', $month), 1);
            $month = strtotime("+1 month", $month);
            if ($month > strtotime('now')) {
                return;
            }
        }
    }

    private function getVendas($mes, $page = 1)
    {
        $dataInicial = "2021-$mes-01";
        $dataFim = date("Y-m-t", strtotime($dataInicial));
        $status_code = 4;
        $dtIniParam = '?registration_date_start=' . $dataInicial;
        $dtFimParam = '&registration_date_end=' . $dataFim;
        $statusParam = "&status=" . $status_code;
        $pageParam = "&page=" . $page;
        $url = 'https://api.hub.sale/2.0/sales/' . $dtIniParam . $dtFimParam . $statusParam . $pageParam;

        $token = $this->getToken();
        $headers = [
            "Content-type: application/json",
            "Authorization: Bearer $token"
        ];
        //printf("\nBuscando de %s ate %s.Page: %s", $dataInicial, $dataFim, $page);
        $response = CustomCurl::sendRequest($url, null, $headers);
        $responseObj = json_decode($response);
        if ($responseObj->success === true) {
            foreach ($responseObj->data as $venda) {
                if ($this->validaVenda($venda)) {
                    $customer = $venda->customer;
                    $cliente = new Cliente();
                    $cliente->nome = $customer->name;
                    $cliente->email = $customer->email;
                    $cliente->telefone = "55" . preg_replace('/[^0-9]/', '', $customer->phone);
                    $cliente->status = "novo";
                    $cliente->transaction_code = $venda->transaction_code;
                    $cliente->json_venda = json_encode($venda);
                    $cliente->save();
                }
            }
            if (count($responseObj->data) > 49) {
                $this->getVendas($mes, ++$page);
            }
        } else {
            echo $response . "\n";
        }
    }

    private function validaVenda($venda): bool
    {
        $itemName = '3 Passos para Um Escritório de Sucesso';
        $itemName = strtolower('3 Passos para Um Escrit');
        foreach ($venda->items as $item) {
            if (strpos(strtolower($item->description), $itemName) !== false) {
                return true;
            }
        }
        return false;
    }
}
