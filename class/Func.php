<?php
class Func
{
    public static function cleanStr($str)
    {
        return trim(preg_replace('!\s+!', ' ', $str));
    }
    public static function cleanFilename($filename)
    {
        return trim(str_replace("/", "", $filename));
    }
    public static function getTodayFilename()
    {
        return str_replace(":", "-", str_replace(" ", "_", date('Y-m-d H:i:s')));
    }
    public static function exportProcessos($filename, $arrayProcesso)
    {
        if (count($arrayProcesso) <= 0) {
            return false;
        }
        $filename = Config::get("general/outputFolder") . Func::getTodayFilename() . "-" . Func::cleanFilename($filename) . ".csv";
        $hfile = fopen($filename, "w");
        fwrite($hfile, $arrayProcesso[0]->getCabecalho() . "\n");
        foreach ($arrayProcesso as $value) {
            fwrite($hfile, $value->getCsv() . "\n");
        }
        fclose($hfile);
    }
}
