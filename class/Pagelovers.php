<?php

class Pagelovers
{
    public static function send(Cliente $cliente): string
    {
        $url = 'https://llapi.leadlovers.com/webapi/lead?token=47BC3367F5194A2A8CC8FA81472A4F76';
        $params = [
            "MachineCode" => '573730',
            "EmailSequenceCode" => '1297622',
            "SequenceLevelCode" => '1',
            "Tag" => '344101',
            "Email" => $cliente->email,
            "Name" => $cliente->nome,
            "Company" => $cliente->id,
            "Source" => "HubSales",
            "Phone" => $cliente->telefone
        ];
        $headers = ['Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6IldlYkFwaSIsInN1YiI6IldlYkFwaSIsInJvbGUiOlsicmVhZCIsIndyaXRlIl0sImlzcyI6Imh0dHA6Ly93ZWJhcGlsbC5henVyZXdlYnNpdGVzLm5ldCIsImF1ZCI6IjFhOTE4YzA3NmE1YjQwN2Q5MmJkMjQ0YTUyYjZmYjc0IiwiZXhwIjoxNjA1NDQxMzM4LCJuYmYiOjE0NzU4NDEzMzh9.YIIpOycEAVr_xrJPLlEgZ4628pLt8hvWTCtjqPTaWMs'];
        return CustomCurl::sendRequest($url, $params, $headers);
    }
}
