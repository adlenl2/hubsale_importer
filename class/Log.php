<?php
class Log
{
    private static $logFolder = "log/";
    public static function info($texto)
    {
        if (Config::get("general/debug")) {
            echo $texto . PHP_EOL;
        }
    }
    public static function logFile($filename, $content)
    {
        if (Config::get("general/debug")) {
            $file = fopen(self::$logFolder . str_replace("/", "", $filename) . ".txt", "w");
            fwrite($file, $content);
            fclose($file);
        }
    }
}
