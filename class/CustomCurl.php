<?php
class CustomCurl
{
    private static $cookie = __DIR__ . "/cookie.txt";
    private static $debug = false;

    public static function clearCookies()
    {
        unlink(self::$cookie);
    }

    public static function sendRequest($url, $params, $headers = [])
    {
        $ch = curl_init();
        if (self::$debug) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, self::$cookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, self::$cookie);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if (!is_null($params)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        sleep(1);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            die(curl_error($ch));
        }
        curl_close($ch);
        return $response;
    }

    public static function get($url)
    {
        return self::sendRequest($url, null);
    }
}
