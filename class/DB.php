<?php

class DB
{
    private static $pdo = null;

    public function __construct()
    {
        if (self::$pdo == null) {
            $host    = Config::get("db/host");
            $db      = Config::get("db/database");
            $user    = Config::get("db/username");
            $pass    = Config::get("db/password");
            $charset = 'utf8';

            $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
            $opt = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            self::$pdo              = new PDO($dsn, $user, $pass, $opt);
        }
    }

    public function getClientes()
    {
        return self::$pdo->query("SELECT * FROM clientes WHERE `status`='novo'")->fetchAll(PDO::FETCH_CLASS, 'Cliente');
    }

    public function insertCliente(Cliente $cliente)
    {
        $stmt = self::$pdo->prepare('SELECT COUNT(*) from clientes WHERE transaction_code = :transaction_code');
        $stmt->execute(array(':transaction_code' => $cliente->transaction_code));

        if($stmt->fetchColumn()){
            return false;
        }
        $insert_stmt = self::$pdo->prepare('INSERT INTO clientes
        (id, email, nome, telefone, `status`, created_at, transaction_code, json_venda)
        VALUES (NULL, ?, ?, ?, ?, ?, ?, ?);');
        $insert_stmt->execute([
            $cliente->email,
            $cliente->nome,
            $cliente->telefone,
            $cliente->status,
            date("Y-m-d H:i:s"),
            $cliente->transaction_code,
            $cliente->json_venda,
        ]);
    }

    public function updateCliente(Cliente $cliente){
        $sql = "UPDATE clientes SET `status`=?, updated_at=?, retorno_leadlovers=? WHERE id=?";
        self::$pdo->prepare($sql)->execute([
            "enviado",
            date("Y-m-d H:i:s"),
            $cliente->retorno_leadlovers,
            $cliente->id
        ]);
    }
}
