<?php
final class Config
{
    private static $init = FALSE;
    private static $config = null;

    public static function get($configName = null)
    {
        self::readConfig();
        if ($configName) {
            //parse path to return config
            $path = explode('/', $configName);
            $tempConfig = self::$config;
            foreach ($path as $element) {
                if (isset($tempConfig[$element])) {
                    $tempConfig = $tempConfig[$element];
                } else {
                    throw new Exception("Configuracao " . $configName . " nao encontrada");
                }
            }
            return $tempConfig;
        }
        throw new Exception("Configuracao " . $configName . " nao encontrada");
    }

    private static function validaParam($secao, $parametro, $valorPadrao)
    {
        if (!isset(self::$config[$secao][$parametro]) || trim(self::$config[$secao][$parametro])  == "") {
            self::$config[$secao][$parametro] = $valorPadrao;
        }
    }

    public static function readConfig()
    {
        if (self::$init == FALSE) {
            self::$config = parse_ini_file(__DIR__."/../config.ini", TRUE, INI_SCANNER_TYPED);
            self::trataData();
            self::trataPaths();
            self::$init = TRUE;
        }
    }
    private static function trataData()
    {
        self::validaParam("general", "periodo", 7);
        self::$config["general"]["dataFinal"] = date('d/m/Y', strtotime('today 00:00:00'));
        self::$config["general"]["dataInicial"] = date('d/m/Y', strtotime('-' . self::$config["general"]["periodo"] . ' days 00:00:00'));
    }
    private static function trataPaths()
    {
        self::validaParam("general", "outputFolder", "");
        /*
        if (self::get("general/outputFolder") != "") {
            tratar / no fim
        }
        */
    }
}
