<?php
class Cliente
{
    public $id;
    public $nome;
    public $email;
    public $telefone;
    public $status;
    public $created_at;
    public $updated_at;
    public $json_venda;
    public $transaction_code;
    public $retorno_leadlovers;

    public static function getAll()
    {
        $db = new DB();
        return $db->getClientes();
    }

    public function save()
    {
        $db = new DB();
        $db->insertCliente($this);
    }

    public function update(){
        $db = new DB();
        $db->updateCliente($this);
    }
}
